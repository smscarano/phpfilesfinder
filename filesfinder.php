<?php
###############
#FilesToDb    #
###############
/*
/Este script busca todos los archivos con cierta extensión en un cierto path e inserta su información en una base de datos MySql.
/Datos a insertar: filename, extension, filesize, md5hash, dirname, hostname, ip
/Tanto la ip como el path deben ser pasados como parámetros en la cli
/Por ejemplo "php filesfinder.php --ip 192.168.1.1 --path /home"
/Se puede pasar una extensión específica como parámetro, de otra manera busca pdfs.
/Por ejemplo "php filesfinder.php --ip 192.168.1.1 --path /home --extension mp3"
*/

###############
#Config  db   #
###############
$options['db']['user'] = 'dbuser';
$options['db']['passwd'] = 'dbpassword';
$options['db']['name'] = 'files';
$options['db']['host'] = 'localhost';

//Cli parámetros
$shortopts  = "";
$shortopts .= "i:";  //ip, obligatorio
$shortopts .= "p:"; // path, valor obligatorio
$shortopts .= "e:"; // extension, valor opcional

$longopts  = [
    "ip:",     // ip, valor obligatorio
    "path:",    // path, valor obligatorio
    "extension:"    //extension, valor opcional
];

$clioptions = getopt($shortopts,$longopts);

//Obtenemos parámetros pasados por consola
//ip
if ( !isset($clioptions['ip']) ) {
	exit('Debe pasar la ip como argumento en la cli con --ip "php filesfinder.php --ip 192.168.1.1" ' . PHP_EOL);
}
$ip = $clioptions['ip'];

//path para utilizar como parámetro de find
if ( !isset($clioptions['path']) ) {
	exit('Debe pasar el path como argumento en la cli con --path "php find.php 192.168.1.1 --path /var/mypath/" ' . PHP_EOL);
}
$searchpath = $clioptions['path'];

//extension
$extension = 'pdf';
if ( isset($clioptions['extension']) ) {
	$extension = $clioptions['extension'];
}

//Hostname
$hostname = gethostname();


############
#Funciones #
############

/*
*connectDb()
*Devuelve un objeto de la db
*/
function connectDb($dboptions){
	
	try {
    	$gbd = new PDO("mysql:host=" . $dboptions['host'] . ";dbname=" . $dboptions['name'] . ";charset=utf8", $dboptions['user'], $dboptions['passwd']);
    	$gbd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $e) {
	    echo $e->getMessage();
	    exit($e->getMessage());
	}
	return $gbd;
}

/*
*insertar()
*Inserta la información de un archivo en la db. Antes chequea que no exista el mismo registro.
*/
function insertar($archivo_array,$gbd){
	
	//Chequear si el archivo existe en la db
	try {
		$sentence = $gbd->prepare("SELECT filename,extension, filesize, md5hash, dirname, hostname, ip FROM files 
			WHERE filename = :filename AND extension = :extension AND filesize = :filesize 
			AND md5hash = :md5hash AND dirname = :dirname AND hostname = :hostname AND ip = :ip");
		$sentence->bindParam(':filename', $archivo_array['filename']);
		$sentence->bindParam(':extension', $archivo_array['extension']);
		$sentence->bindParam(':filesize', $archivo_array['filesize']);
		$sentence->bindParam(':md5hash', $archivo_array['md5hash']);
		$sentence->bindParam(':dirname', $archivo_array['dirname']);
		$sentence->bindParam(':hostname', $archivo_array['hostname']);
		$sentence->bindParam(':ip', $archivo_array['ip']);
		$sentence->execute();
		$result = $sentence->fetchAll();
    }
	catch (PDOException $e) {
	    echo 'Query failed: ' . $e->getMessage();
	}

	if ( count($result) >= 1 ) {
		return 1;
	}

	//Si el archivo no existe en la db insertar
	try {
		$sentence = $gbd->prepare("INSERT INTO files (filename, extension, filesize, md5hash, dirname, hostname, ip) 
		VALUES (:filename, :extension, :filesize, :md5hash, :dirname, :hostname, :ip)");
		$sentence->bindParam(':filename', $archivo_array['filename']);
		$sentence->bindParam(':extension', $archivo_array['extension']);
		$sentence->bindParam(':filesize', $archivo_array['filesize']);
		$sentence->bindParam(':md5hash', $archivo_array['md5hash']);
		$sentence->bindParam(':dirname', $archivo_array['dirname']);
		$sentence->bindParam(':hostname', $archivo_array['hostname']);
		$sentence->bindParam(':ip', $archivo_array['ip']);
		$sentence->execute();
	}

	catch (PDOException $e) {
		echo 'Query failed: ' . $e->getMessage();
	}
	return 2;
}

/*
*procesarArchivos()
*Obtiene información de los archivos, la procesa y luego la inserta
*/
function procesarArchivos($archivos,$hostname,$ip,$gbd){
	$contador = 1;
	$total_archivos = count($archivos);
	foreach ($archivos as $key => $archivo) {
		if ( empty($archivo) ) {
			continue;
		}
		
		echo "Procesando " . $contador . "/" . $total_archivos . PHP_EOL;
		
		//Generar array con datos de cada archivo
		$pathinfo = pathinfo($archivo);
		$archivo_array['filesize'] = filesize($archivo);
		$archivo_array['md5hash'] =  md5_file($archivo);
		$archivo_array['extension'] = $pathinfo['extension'];
		$archivo_array['dirname'] = $pathinfo['dirname'];
		$archivo_array['filename'] = $pathinfo['basename'];
		$archivo_array['hostname'] = $hostname;
		$archivo_array['ip'] = $ip;
		$archivo_array['fullpath'] = $archivo_array['dirname'] . "/" . $archivo_array['filename'];

		//Insertar datos del archivo en la db
		$insert = insertar($archivo_array,$gbd);
		
		if ($insert == 1) {
			echo "El archivo " . $archivo_array['fullpath'] . " ya existe en la db" . PHP_EOL;
			$existen['si'][] = 1;
		}
		if ($insert == 2) {
			echo "El archivo " . $archivo_array['fullpath'] . " se insertó en la db" . PHP_EOL;
			$existen['no'][] = 2;
		}
		
		echo PHP_EOL;
		$contador++;
	}
	return $existen;
}

###########
#Ejecución#
###########

//Conexión db
$gbd = connectDb($options['db']);

//Inicio búsqueda y procesamiento
$date_inicio = date("d/m/Y H:i:s");
echo "Iniciando proceso búsqueda a las $date_inicio" . PHP_EOL;

//Buscar archivos en $searchpath o / si $searchpath no está definido
if (isset($searchpath)) {
 	$salida_shell = shell_exec('find ' . $searchpath . ' -name *.' . $extension);
} 
else{
	$salida_shell = shell_exec('find / -name *.' . $extension);
}

//Obtener array del fullpath de cada archivo
$archivos = explode(PHP_EOL,$salida_shell);
unset($archivos[count($archivos)-1]);//Find devuelve la última línea vacía

echo "Finalizado proceso búsqueda a las " . date("d/m/Y H:i:s") . PHP_EOL;
echo "Se encontraron " . count($archivos) . " archivos" . PHP_EOL;
echo "Iniciando procesamiento archivos a las " . date("d/m/Y H:i:s") . PHP_EOL;

//Procesar array archivos
$salidaproceso = procesarArchivos($archivos,$hostname,$ip,$gbd);

//Fin proceso
$date_final = date("d/m/Y H:i:s");
$duracion = strtotime($date_final) - strtotime($date_inicio);

if ( !isset($salidaproceso['si']) ) {
	$salidaproceso['si'] = 0;
}
if ( !isset($salidaproceso['no']) ) {
	$salidaproceso['no'] = 0;
}

echo "Proceso finalizado a las $date_final" . PHP_EOL;
echo "Se procesaron " . count($archivos) . " registros". PHP_EOL;
echo count($salidaproceso['si']) . " registros existían en la db". PHP_EOL;
echo count($salidaproceso['no']) . " registros no existían en la db". PHP_EOL;
echo "Duración del proceso: $duracion segundos, " . number_format($duracion/60 ,2,',','.') . " minutos, " . number_format($duracion/60/60 ,2,',','.') . " horas" . PHP_EOL;
