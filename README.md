# PHPFILESFINDER
**Funcionalidad:** Busca los archivos con cierta extensión en un cierto path e inserta su información en una base de datos MySql  
**Requisitos:**  
  * **SO:** UNIX que cuente con el comando find  
  * **DB:** MySQL  
  * **Permisos:** De lectura en el path a buscar y permisos para crear y escribir en la base de datos  
**Datos de archivo a insertar:** filename, extension, filesize, md5hash, dirname, hostname, ip  
**Parámetros:** Tanto la ip como el path deben ser pasados como parámetros en la cli  
```
#!php

php filesfinder.php --ip 192.168.1.1 --path /home
```  

Se puede pasar una extensión específica como parámetro. Por defecto busca pdfs.  
```
#!php

php filesfinder.php --ip 192.168.1.1 --path /home --extension mp3
```
**Base de datos**
Tanto el script sql para crear la base de datos como el esquema de MySql workbench se encuentran en el repositorio.  
Para crear la base de datos se debe correr desde consola:  

```
#!bash

mysql -umysqluser -p < dbcreatre.sql
```