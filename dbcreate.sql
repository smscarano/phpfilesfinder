-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema files
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema files
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `files` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `files` ;

-- -----------------------------------------------------
-- Table `files`.`files`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `files`.`files` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `filename` VARCHAR(255) NOT NULL COMMENT '',
  `extension` VARCHAR(45) NOT NULL COMMENT '',
  `filesize` BIGINT UNSIGNED NOT NULL COMMENT '',
  `md5hash` CHAR(32) NOT NULL COMMENT '',
  `dirname` VARCHAR(255) NOT NULL COMMENT '',
  `hostname` VARCHAR(145) NOT NULL COMMENT '',
  `ip` VARCHAR(15) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
